# Stm Core

A compendium of functions and structures that I've accrued while working on STM32 based systems. Some more useful than others, all ultimately gathered so I have a central repo to maintain, versus 5 different iterations.